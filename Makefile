.PHONY: help migrate makemigrations newapp docker-build docker-rebuild docker-prod-build

help: ## Display this help message
	@echo "Usage: make [command]"
	@echo ""
	@echo "Commands:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "  %-20s %s\n", $$1, $$2}'

migrate: ## Apply database migrations
	docker-compose -f docker/docker-compose.dev.yml run django python manage.py migrate

makemigrations: ## Create new database migrations
	docker-compose -f docker/docker-compose.dev.yml run django python manage.py makemigrations

newapp: ## Create a new Django app. Usage: make newapp name=<app_name>
	docker-compose -f docker/docker-compose.dev.yml run django python manage.py startapp $(name)

collectstatic: ## Create a new Django app. Usage: make newapp name=<app_name>
	docker-compose -f docker/docker-compose.dev.yml run django python manage.py collectstatic

createsuperuser: ## Create a new Django app. Usage: make newapp name=<app_name>
	docker-compose -f docker/docker-compose.dev.yml run django python manage.py createsuperuser

build: ## Build Docker containers for development
	docker-compose -f docker/docker-compose.dev.yml build

build-staging: ## Build Docker containers for development
	docker-compose -f docker/docker-compose.staging.yml build

build-prod: ## Build Docker containers for production
	docker-compose -f docker/docker-compose.prod.yml build

rebuild: ## Rebuild Docker containers for development (without cache)
	docker-compose -f docker/docker-compose.dev.yml build --no-cache

up: ## Run server using dev config
	docker-compose -f docker/docker-compose.dev.yml up

up-staging: ## Run server using staging config
	docker-compose -f docker/docker-compose.staging.yml up

up-prod: ## Run server using prod config
	docker-compose -f docker/docker-compose.prod.yml up

up-prod-detached: ## Run server using prod config
	docker-compose -f docker/docker-compose.prod.yml up -d

up-staging-detached: ## Run server using prod config
	docker-compose -f docker/docker-compose.staging.yml up -d

down: ## Shut Down server using dev config
	docker-compose -f docker/docker-compose.dev.yml down

down-staging: ## Shut Down server using staging config
	docker-compose -f docker/docker-compose.staging.yml down

down-prod: ## Shut Down server using prod config
	docker-compose -f docker/docker-compose.prod.yml down
