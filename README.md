# Django Project with Docker, AWS, and SSL
This repository contains a template for a Django application with Docker, separate environments for development and production, and deployment instructions for AWS EC2 with SSL certificates.

## Project Structure

## Getting Started

### Prerequisites

- Docker
- Docker Compose
- AWS CLI
- Certbot (for SSL certificates)
- Makefile

### Setup Makefile
- Read The makefile for useful commands, these commands will be used in the setup below.

### Setup

1. **Clone the repository:**

   ```bash
   git clone https://github.com/your-repo/my_project.git
   cd my_project

2. Set up environment variables:
- Create .env.dev, .env.staging, and .env.prod files with the necessary environment variables.

3. Build Docker containers for development
- `make build` or `docker-compose -f docker/docker-compose.dev.yml up`

4. Run the development server:
- `make up` or `docker-compose -f docker/docker-compose.dev.yml build`

### Deployment
## Production Setup on AWS EC2
# Read the bottom instructions to setup security groups on ec2 instance.
# Launch EC2 Instances:

- One for production.
- One for staging.

# Connect to Each Instance:
- `ssh -i /path/to/your-key-pair.pem ec2-user@your-production-ec2-public-dns`
- `ssh -i /path/to/your-key-pair.pem ec2-user@your-staging-ec2-public-dns`

<!-- Possibly change EC2 instance to ubuntu for less installs? -->
# Install Docker and Docker Compose on Both Instances: 
- `sudo yum update -y`
- `sudo yum install docker -y`
- `sudo yum install python3`
- `sudo systemctl start docker`
- `sudo systemctl enable docker`
- `sudo usermod -a -G docker ec2-user`
- `sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
- `sudo chmod +x /usr/local/bin/docker-compose`
- `sudo yum install git -y`
- `sudo dnf install libxcrypt-compat`
- `curl -O https://bootstrap.pypa.io/get-pip.py`
- `python3 get-pip.py --user`

# Exit and log back in to instance to reflect chagnes to user
- `ssh -i /path/to/your-key-pair.pem ec2-user@your-ec2-public-dns`

# Clone Your Repository on Both Instances:
- `git clone https://github.com/your-repo/my_project.git`
- `cd my_project`

# Set Up Environment Variables:
- Create .env/ folder
- Create `.env/dev`, `.env/staging`, and `.env/prod` files with the necessary environment variables.


# Run Docker Compose on Both Instances:
- `docker-compose -f docker/docker-compose.prod.yml up -d`
- `docker-compose -f docker/docker-compose.staging.yml up -d`

# Setting Up Domain with Route 53
## Step 1: Create a Hosted Zone in Route 53
1. Sign in to the AWS Management Console.
2. Navigate to Route 53 by selecting "Route 53" under the "Networking & Content Delivery" section.
3. In the Route 53 dashboard, click on "Hosted zones" in the left-hand menu.
4. Click the "Create hosted zone" button.
5. Fill out the "Create Hosted Zone" form:
   - **Domain Name**: Enter your domain name (e.g., `example.com`).
   - **Type**: Select "Public hosted zone".
   - **Comment**: (Optional) Add a comment for reference.
6. Click "Create hosted zone".

## Step 2: Update Your Domain Registrar's Settings
1. After creating the hosted zone, note the list of name servers (NS records) provided by Route 53.
2. Go to your domain registrar's website where you purchased your domain.
3. Find the settings to update the name servers for your domain, often under "DNS Settings" or "Nameservers".
4. Replace the existing name servers with the Route 53 name servers listed in the NS records.
5. Save the changes. DNS propagation can take up to 48 hours to fully propagate these changes.

## Step 3: Add A Records in Route 53
1. Obtain the public IP addresses of your EC2 instances from the EC2 dashboard.
2. In the Route 53 console, select your hosted zone from the "Hosted zones" list.
3. Click on the "Create record" button.

### Create A Record for Production
1. Enter the subdomain for production in the "Record name" field (e.g., `www.example.com` or `prod.example.com`).
2. Select "A - IPv4 address" for "Record type".
3. Enter the public IP address of your production EC2 instance in the "Value" field.
4. Click "Create records".

### Create A Record for Staging
1. Enter the subdomain for staging in the "Record name" field (e.g., `staging.example.com`).
2. Select "A - IPv4 address" for "Record type".
3. Enter the public IP address of your staging EC2 instance in the "Value" field.
4. Click "Create records".
5. Repeat for AAA records

# Install Certbot on Both Instances:
- `sudo yum install -y certbot`

# Stop Your Docker Containers Temporarily on Both Instances:
- `docker-compose -f docker/docker-compose.prod.yml down`
- `docker-compose -f docker/docker-compose.staging.yml down`

# Run Certbot to Obtain SSL Certificates on Both Instances:
- The below commands will generate SSL certificates and store them in /etc/letsencrypt/live/your-domain.com/ and /etc/letsencrypt/live/staging.your-domain.com/.
   - `sudo certbot certonly --standalone -d your-domain.com -d www.your-domain.com`
   - `sudo certbot certonly --standalone -d staging.your-domain.com -d www.staging.your-domain.com`

# Set Up Cron Jobs to Renew SSL Certificates on Both Instances:
Certbot should do this for you already, but if not do something similar like below:

- Create a script renew_certificates.sh as shown above.
- Make the script executable: chmod +x renew_certificates.sh.
- Edit the crontab to run the script periodically: sudo crontab -e and add the cron job.
- Restart Your Application on Both Instances:
   - `docker-compose -f docker/docker-compose.prod.yml up -d`
   - `docker-compose -f docker/docker-compose.staging.yml up -d`

# Verify Your Setup:
- Visit your production and staging domains to verify that HTTPS is working and the SSL certificates are correctly installed.

# CI/CD with GitLab
- Add the provided .gitlab-ci.yml to your repository.
- Configure your GitLab project to use the GitLab CI/CD pipeline.

This setup provides a comprehensive guide to getting your Django application up and running in both development and production environments, with automated commands and deployment instructions for AWS EC2.

This `Makefile` and `README.md` should now provide a clear and concise way to manage your Django project, including setup and deployment instructions.

# Configuring Security Groups for Django Application on EC2

To run a Django application on an EC2 instance, you'll need to configure the security groups to allow the necessary traffic. Below are the typical rules you should set up:

## Security Group Rules

### HTTP (Port 80)
- **Type**: HTTP
- **Protocol**: TCP
- **Port Range**: 80
- **Source**: Anywhere (0.0.0.0/0) if you want your site to be publicly accessible

### HTTPS (Port 443)
- **Type**: HTTPS
- **Protocol**: TCP
- **Port Range**: 443
- **Source**: Anywhere (0.0.0.0/0) if you want your site to be publicly accessible

### SSH (Port 22)
- **Type**: SSH
- **Protocol**: TCP
- **Port Range**: 22
- **Source**: Your IP (for security purposes, it's best to restrict SSH access to your specific IP address)

### Custom TCP (For Django development server, typically Port 8000)
- **Type**: Custom TCP Rule
- **Protocol**: TCP
- **Port Range**: 8000 (or whatever port you configured Django to run on)
- **Source**: Anywhere (0.0.0.0/0) or your IP, depending on your use case

### Database Access (Optional, if you are accessing a database directly)
- **Type**: Custom TCP Rule
- **Protocol**: TCP
- **Port Range**: 5432 for PostgreSQL, 3306 for MySQL, etc.
- **Source**: Your IP or the IP range of your application servers

## Steps to Add Security Group Rules

1. **Navigate to the EC2 Dashboard**:
   - Go to the AWS Management Console and open the EC2 Dashboard.

2. **Select Your Instance**:
   - Select the instance running your Django application.

3. **Modify Security Groups**:
   - Under the "Description" tab, find the "Security groups" section and click on the security group linked to your instance.

4. **Add Inbound Rules**:
   - Click on the "Inbound rules" tab and then click the "Edit inbound rules" button.
   - Add the necessary rules as mentioned above.

5. **Save Rules**:
   - After adding the rules, click "Save rules" to apply the changes.

Ensure you have the proper security measures in place, such as restricting SSH access to your IP address and using HTTPS for secure communication.