"""Urls for app file goes here."""
from django.urls import path
from django.views.generic.base import TemplateView


urlpatterns = [
    path('', TemplateView.as_view(template_name='app/home.html'), name='home'),
]
