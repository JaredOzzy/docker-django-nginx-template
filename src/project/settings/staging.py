from .base import *

# Staging-specific settings
DEBUG = True

ALLOWED_HOSTS = ['staging.your-domain.com']

# Additional staging settings...