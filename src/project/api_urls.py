"""Project Urls."""
from rest_framework import routers
from app.api import views

urlpatterns = [
]
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

urlpatterns += router.urls

